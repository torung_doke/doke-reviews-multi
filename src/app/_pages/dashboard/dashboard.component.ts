import { Component, OnInit } from '@angular/core';
import { NotifyService } from 'src/app/_services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private notify: NotifyService) { }

  ngOnInit() {
  }

  onSucess(){
    this.notify.isSuccess()
  }
}
