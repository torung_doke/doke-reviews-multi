import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import * as moment from 'moment';
// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "../../_share/charts";

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {

  public datasets: any;
  public data: any;
  public salesChart;
  public clicked: boolean = true;
  public clicked1: boolean = false;

  customRanges = [
    {
      text: 'Hôm nay', desc: 'Hôm nay', value: 'today',
      start: moment(),
      end: moment(),
      default: true
    },
    {
      text: 'Hôm qua', desc: 'Hôm qua', value: 'lastday',
      start: moment().subtract(1, 'days').startOf('days'),
      end: moment().subtract(1, 'days').endOf('days')
    },
    {
      text: '7 ngày qua', desc: '7 ngày qua', value: 'last7day',
      start: moment().subtract(6, 'days').startOf('days'),
      end: moment().endOf('days')
    },
    {
      text: '30 ngày qua', desc: '30 ngày qua', value: 'last30day',
      start: moment().subtract(29, 'days').startOf('days'),
      end: moment().endOf('days')
    },
    {
      text: 'Tháng này', desc: 'Tháng này', value: 'thismonth',
      start: moment().startOf('month'),
      end: moment().endOf('month')
    },
    {
      text: 'Tháng trước', desc: 'Tháng trước', value: 'lastmonth',
      start: moment().subtract(1, 'month').startOf('month'),
      end: moment().subtract(1, 'month').endOf('month')
    }
  ];

  constructor() { }

  ngOnInit() {

    this.datasets = [
      [0, 20, 10, 30, 15, 40, 20, 60, 60],
      [0, 20, 5, 25, 10, 30, 15, 40, 40]
    ];
    this.data = this.datasets[0];

    parseOptions(Chart, chartOptions());
    /**
     * CHART REVIEWS
     */
    var chartSales = document.getElementById('chart-sales');

    this.salesChart = new Chart(chartSales, {
			type: 'line',
			options: chartExample1.options,
			data: chartExample1.data
    });

    /**
     * CHART ORDERS
     */
    // var chartOrders = document.getElementById('chart-orders');
    
    // var ordersChart = new Chart(chartOrders, {
    //   type: 'bar',
    //   options: chartExample2.options,
    //   data: chartExample2.data
    // });

    /**
     * CHART DEVICES
     */
    var dataDevices = {
      datasets: [{
          label: 'Device reviews',
          data: [10, 8, 30],
          backgroundColor: ["rgb(5, 48, 102)", "rgb(54, 162, 235)", "rgb(255, 205, 86)"]
      }],
  
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: [
          'Desktop',
          'Tablet',
          'Mobile'
      ]
    };
    var chartDevices = document.getElementById('chart-devices');
    var myPieChart = new Chart(chartDevices, {
      type: 'pie',
      data: dataDevices,
      options: {

      }
    });

    /**
     * CHART STATUS
     */
    var dataDevices = {
      datasets: [{
          label: 'Status reviews',
          data: [90, 10],
          backgroundColor: ["rgb(54, 162, 235)", "#ddd"]
      }],
  
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: [
          'Hiển thị',
          'Đang chờ'
      ]
    };
    var chartDevices = document.getElementById('chart-status');
    var myPieChart = new Chart(chartDevices, {
      type: 'pie',
      data: dataDevices,
      options: {

      }
    });

  }





  public updateOptions() {
    this.salesChart.data.datasets[0].data = this.data;
    this.salesChart.update();
  }

  isFilterByDate(){

  }
}
