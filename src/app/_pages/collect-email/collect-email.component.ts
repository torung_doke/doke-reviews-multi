import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-collect-email',
  templateUrl: './collect-email.component.html',
  styleUrls: ['./collect-email.component.css']
})
export class CollectEmailComponent implements OnInit {
  dataBody$: Subject<string> = new Subject();
  dataCallToAction$: Subject<string> = new Subject();
  dataSignal$: Subject<string> = new Subject();
  @ViewChild('iframe') iframe: ElementRef;

  viewMode = [500,902];

  previewHTML: string = `
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        @media only screen and (max-width: 320px) {
            .desktop {
              display: none;
            }
            .mobile {
              display: block !important;
              max-height: none !important;
              font-size: 100% !important;
              overflow: visible !important;
            }
          }
    </style>
    <script type="text/javascript">
      if (emailBody) {
        emailBody.subscribe(res => {
          document.querySelector('.content-header').innerHTML = res;
        })
      }
      if (callToAction) {
        callToAction.subscribe(res => {
          document.querySelector('.call-to-action-text').innerHTML = res;
        })
      }
      if (signal) {
        signal.subscribe(res => {
          document.querySelector('.footer-signal').innerHTML = res;
        })
      }
      </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="font-family: Arial; color: rgb(103, 106, 108); margin: 0px;">
    <div class="main-div" style="background-color:#eaeced;width:100%;margin:0;padding:30px 0 30px 0;font-size:15px">
        <div class="gray-text-centered" style="text-align:center;color:#97a6b4;line-height:14px;font-size:11px;letter-spacing:-0.22px">
            <p class="displaying-incorrectly" style="font-size:11px">If this email is not displaying correctly, please
                <a href="https://yotpo.com/go/p1cVfJSB?submission_type=broken_email" style="text-decoration:none;color:#2f84ed">click here</a> to fill out the form.</p>
        </div>
        <table cellspacing="0" class="main-table" style="background-color:#ffffff;width:100%;max-width:600px;margin:0 auto;border:0.1em solid #c8c8c8;border-radius:5px;-webkit-border-radius:5px;-moz-border-radius:5px">
            <tbody>
                <tr>
                    <td class="main-td" style="font-family:Arial;color:#676a6c;padding:0">
                        <div class="header-logo" style="width:100%;padding-top:40px;padding-bottom:40px;border-top-right-radius:5px;border-top-left-radius:5px;-moz-border-radius-topright:5px;-moz-border-radius-topleft:5px;-webkit-border-top-right-radius:5px;-webkit-border-top-left-radius:5px;text-align:center"><img src="https://s3.amazonaws.com/yotpo-images-production/images/store_def_logo.png" alt="Logo" class="logo-img" style="max-width:200px;max-height:200px"></div>
                        <div class="content-header" style="padding:40px 15px 20px;letter-spacing:0.21px;line-height:21px;font-size:14px;color:#676a6c;text-align:left">
                            Hello {user},<br>You've recently bought <a href="http://doke-dev.myshopify.com" style="text-decoration:none;color:#2f84ed">{product}</a>, what do you think about it?

                        </div>
                        <div class="yotpo-readonly-start"></div>
                        <div class="desktop">
                            <!--[if (mso)]><div class="yotpo-readonly-start"></div>
<div style="text-align:center;font-family:Arial;">
  <div style="width:99%;max-width:600px;background:#fff;margin:20px auto">
    <div style="text-align:center;font-size:14px;padding-bottom:18px;margin-bottom:18px">
      <table>
        <tbody>
        <tr>
          <td style="font-family:Arial;color:#676a6c"></td>
          <td width="100%" style="font-family:Arial;color:#676a6c">
            <div style="text-align:center;font-size:14px;">
              <span class="call-to-action-text yotpo-not-readonly" style="font-size:20px;color:#56575f">
                Click below to rate (1-5):
              </span>
            </div>
          </td>
          <td width="15%" style="font-family:Arial;color:#676a6c"></td>
        </tr>
        </tbody>
      </table>
      <table width="100%">
        <tr>
          <td width="25%" style="font-family:Arial;color:#676a6c"></td>
<td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
              <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=1" target="_blank" style="text-decoration:none;color:#2f84ed">
                <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                  ☆
                </div>
                <div class="formless-star-value" style="color:black;font-size:10px">1</div>
              </a>
            </td>
<td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
              <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=2" target="_blank" style="text-decoration:none;color:#2f84ed">
                <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                  ☆
                </div>
                <div class="formless-star-value" style="color:black;font-size:10px">2</div>
              </a>
            </td>
<td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
              <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=3" target="_blank" style="text-decoration:none;color:#2f84ed">
                <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                  ☆
                </div>
                <div class="formless-star-value" style="color:black;font-size:10px">3</div>
              </a>
            </td>
<td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
              <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=4" target="_blank" style="text-decoration:none;color:#2f84ed">
                <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                  ☆
                </div>
                <div class="formless-star-value" style="color:black;font-size:10px">4</div>
              </a>
            </td>
<td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
              <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=5" target="_blank" style="text-decoration:none;color:#2f84ed">
                <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                  ☆
                </div>
                <div class="formless-star-value" style="color:black;font-size:10px">5</div>
              </a>
            </td>
<td width="25%" style="font-family:Arial;color:#676a6c"></td>
        </tr>
      </table>
    </div>
  </div>
</div>
<div class="yotpo-readonly-end"></div><![endif]-->
                            <!--[if !mso]><!-->
                            <div class="main-form" style="width:100%;padding:10px 0 20px 0;height:auto;min-height:360px;color:#676a6c">
                                <form action="https://doke.vn/review" method="get" target="_blank">
                                    <input type="hidden" name="reviewer_token" value="fake" style="font-family:Arial;color:#676a6c"><input type="hidden" name="appkey" value="Ick5yCkY0EVf2BvVluYDVcunM1dgFAJyYS2Uej2v" style="font-family:Arial;color:#676a6c">
                                    <input
                                        type="hidden" name="email_type" value="reminder" style="font-family:Arial;color:#676a6c"><input type="hidden" name="utm_source" value="map" style="font-family:Arial;color:#676a6c"><input type="hidden" name="utm_medium" value="email" style="font-family:Arial;color:#676a6c"><input type="hidden" name="utm_campaign"
                                            value="map_traffic_success_failure_delayed" style="font-family:Arial;color:#676a6c"><input type="hidden" name="skip_share" value="false" style="font-family:Arial;color:#676a6c"><input type="hidden" name="review_images"
                                            value="false" style="font-family:Arial;color:#676a6c"><input type="hidden" name="review_title_required" value="true" style="font-family:Arial;color:#676a6c"><input type="hidden" name="review_accept_one_field" value="true"
                                            style="font-family:Arial;color:#676a6c"><input type="hidden" name="domain_key" value="fake" style="font-family:Arial;color:#676a6c"><input type="hidden" name="mail_language" value="en" style="font-family:Arial;color:#676a6c">
                                        <input
                                            type="hidden" name="demo" value="true" style="font-family:Arial;color:#676a6c"><input type="hidden" name="submission_type" value="in_mail_form" style="font-family:Arial;color:#676a6c">
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="5%" style="font-family:Arial;color:#676a6c"></td>
                                                        <td class="input-field-title" style="font-family:Arial;text-align:left;font-size:12px;letter-spacing:0;color:#676a6c;padding:5px 0;font-weight:700">
                                                            Xếp hạng:
                                                        </td>
                                                        <td width="5%" style="font-family:Arial;color:#676a6c"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="5%" style="font-family:Arial;color:#676a6c"></td>
                                                        <td width="90%" class="stars-container" style="font-family:Arial;color:#676a6c;color:#FDC705">
                                                            <span class="stars-container-top" style="display:inline-block"><span class="stars-radio-button" style="display:inline-block;float:left;margin:0px 15px 0px 0px">
  <input name="review_score" type="radio" value="1" style="font-family:Arial;color:#676a6c;margin:0px 2px 0px 0px">
  <label>
    <span class="stars-input" style="font-size:16px;letter-spacing:0.21em;padding-left:0">★</span>
                                                            </label>
                                                            </span>
                                                            <span class="stars-radio-button" style="display:inline-block;float:left;margin:0px 15px 0px 0px">
  <input name="review_score" type="radio" value="2" style="font-family:Arial;color:#676a6c;margin:0px 2px 0px 0px">
  <label>
    <span class="stars-input" style="font-size:16px;letter-spacing:0.21em;padding-left:0">★★</span>
                                                            </label>
                                                            </span>
                                                            <span class="stars-radio-button" style="display:inline-block;float:left;margin:0px 15px 0px 0px">
  <input name="review_score" type="radio" value="3" style="font-family:Arial;color:#676a6c;margin:0px 2px 0px 0px">
  <label>
    <span class="stars-input" style="font-size:16px;letter-spacing:0.21em;padding-left:0">★★★</span>
                                                            </label>
                                                            </span>
                                                            </span>
                                                            <span class="stars-container-bottom" style="display:inline-block"><span class="stars-radio-button" style="display:inline-block;float:left;margin:0px 15px 0px 0px">
  <input name="review_score" type="radio" value="4" style="font-family:Arial;color:#676a6c;margin:0px 2px 0px 0px">
  <label>
    <span class="stars-input" style="font-size:16px;letter-spacing:0.21em;padding-left:0">★★★★</span>
                                                            </label>
                                                            </span>
                                                            <span class="stars-radio-button" style="display:inline-block;float:left;margin:0px 15px 0px 0px">
  <input name="review_score" type="radio" value="5" checked="" style="font-family:Arial;color:#676a6c;margin:0px 2px 0px 0px">
  <label>
    <span class="stars-input" style="font-size:16px;letter-spacing:0.21em;padding-left:0">★★★★★</span>
                                                            </label>
                                                            </span>
                                                            </span>
                                                        </td>
                                                        <td width="5%" style="font-family:Arial;color:#676a6c"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="5%" style="font-family:Arial;color:#676a6c"></td>
                                                        <td width="90%" style="font-family:Arial;color:#676a6c">
                                                            <table cellspacing="0" cellpadding="0" border="0" class="full-width" style="width:100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="input-field-title" style="font-family:Arial;text-align:left;font-size:12px;letter-spacing:0;color:#676a6c;padding:5px 0;font-weight:700">
                                                                            Tiêu đề đánh giá:
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-family:Arial;color:#676a6c">
                                                                            <ul style="margin:0;padding:0;">
                                                                                <input class="text-field" type="text" name="review_title" style="font-family:Arial;color:#676a6c;width:99%;padding-left:3px;height:33px;font-size:1em;border:solid 1px #dbddde;font-weight:lighter;border-radius:5px;-webkit-border-radius:5px;-moz-border-radius:5px"><br>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="input-field-title" width="100%" style="font-family:Arial;text-align:left;font-size:12px;letter-spacing:0;color:#676a6c;padding:5px 0;font-weight:700">
                                                                            Nội dung đánh giá:
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-family:Arial;color:#676a6c">
                                                                            <ul style="margin:0;padding:0;">
                                                                                <textarea class="textarea" name="review_content" cols="40" rows="5" style="font-family:Arial;color:#676a6c;width:99%;padding-left:3px;height:60px;font-size:1em;border:solid 1px #dbddde;font-weight:lighter;border-radius:5px;-webkit-border-radius:5px;-moz-border-radius:5px"></textarea><br>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="col-centered" style="font-family:Arial;color:#676a6c;text-align:center">
                                                                            <button type="submit" class="submit" style="font-family:Arial;color:#676a6c;-webkit-appearance:none;-moz-border-radius:3px;-webkit-border-radius:3px;color:#ffffff;background-color:#1cc286;border-radius:3px;padding:6px 13px;font-size:14px;font-weight:bold;border:none;text-decoration:none;min-height:36px;min-width:220px;margin-top:15px">
                                                                            Gửi đánh giá
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                        <td width="5%" style="font-family:Arial;color:#676a6c"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                </form>
                            </div>
                            <!--<![endif]-->
                        </div>

                        <!--[if !mso]><!-->
                        <div class="mobile" style="display:none; max-height: 0px; font-size: 0px; overflow: hidden;">
                            <div class="yotpo-readonly-start"></div>
                            <div style="text-align:center;font-family:Arial;">
                                <div style="width:99%;max-width:600px;background:#fff;margin:20px auto">
                                    <div style="text-align:center;font-size:14px;padding-bottom:18px;margin-bottom:18px">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td style="font-family:Arial;color:#676a6c"></td>
                                                    <td width="100%" style="font-family:Arial;color:#676a6c">
                                                        <div style="text-align:center;font-size:14px;">
                                                            <span class="call-to-action-text yotpo-not-readonly" style="font-size:20px;color:#56575f">
                Click below to rate (1-5):
              </span>
                                                        </div>
                                                    </td>
                                                    <td width="15%" style="font-family:Arial;color:#676a6c"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td width="25%" style="font-family:Arial;color:#676a6c"></td>
                                                    <td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
                                                        <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=1" target="_blank" style="text-decoration:none;color:#2f84ed">
                                                            <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                                                                ☆
                                                            </div>
                                                            <div class="formless-star-value" style="color:black;font-size:10px">1</div>
                                                        </a>
                                                    </td>
                                                    <td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
                                                        <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=2" target="_blank" style="text-decoration:none;color:#2f84ed">
                                                            <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                                                                ☆
                                                            </div>
                                                            <div class="formless-star-value" style="color:black;font-size:10px">2</div>
                                                        </a>
                                                    </td>
                                                    <td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
                                                        <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=3" target="_blank" style="text-decoration:none;color:#2f84ed">
                                                            <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                                                                ☆
                                                            </div>
                                                            <div class="formless-star-value" style="color:black;font-size:10px">3</div>
                                                        </a>
                                                    </td>
                                                    <td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
                                                        <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=4" target="_blank" style="text-decoration:none;color:#2f84ed">
                                                            <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                                                                ☆
                                                            </div>
                                                            <div class="formless-star-value" style="color:black;font-size:10px">4</div>
                                                        </a>
                                                    </td>
                                                    <td width="10%" style="font-family:Arial;color:#676a6c;text-align: center">
                                                        <a href="https://yotpo.com/go/p1cVfJSB?submission_type=formless&amp;review_score=5" target="_blank" style="text-decoration:none;color:#2f84ed">
                                                            <div class="stars-container formless-star yotpo-not-readonly" style="color:#FDC705;font-size:50px;width:100%;height:100%">
                                                                ☆
                                                            </div>
                                                            <div class="formless-star-value" style="color:black;font-size:10px">5</div>
                                                        </a>
                                                    </td>
                                                    <td width="25%" style="font-family:Arial;color:#676a6c"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="yotpo-readonly-end"></div>
                        </div>
                        <!--<![endif]-->
                        <div class="yotpo-readonly-end"></div>
                        <div class="content-bottom gray-text-centered" style="background-color:#f8f8f8;padding:10px 15px;border-top:1px solid #cccccc;border-bottom-right-radius:5px;border-bottom-left-radius:5px;-moz-border-radius-bottomright:5px;-moz-border-radius-bottomleft:5px;-webkit-border-bottom-right-radius:5px;-webkit-border-bottom-left-radius:5px;text-align:center;color:#97a6b4;line-height:14px;font-size:11px;letter-spacing:-0.22px">
                            <span class="footer-signal">We really appreciate your feedback and hope to see you again soon.</span>
                        </div>

                    </td>
                </tr>
            </tbody>
        </table>
        <div class="unsubscribe gray-text-centered" style="width:100%;margin:0 auto;text-align:center;color:#97a6b4;line-height:14px;font-size:11px;letter-spacing:-0.22px">
            <p style="margin-bottom:0">
                To unsubscribe click
                <a href="/" style="text-decoration:none;color:#2f84ed">here</a>
            </p>
        </div>
        <div class="powered-by gray-text-centered" style="width:100%;margin:0 auto;padding:0 0 10px 0;font-weight:bold;letter-spacing:0.5px;text-align:center;color:#97a6b4;line-height:14px;font-size:11px;letter-spacing:-0.22px">
            <p>
                <a href="doke.vn" style="text-decoration:none;color:#2f84ed">Powered by DOKE</a>
            </p>
        </div>
    </div>
</body>
</html>
  `

  emailBody : string = 'Xin chào {user},\n<br>Gần đây bạn đã mua {product}, bạn nghĩ gì về nó?';
  callToAction: string = 'Nhấp vào bên dưới để xếp hạng (1-5):';
  signal: string = 'Chúng tôi thực sự đánh giá cao phản hồi của bạn và hy vọng sẽ gặp lại bạn sớm.\nCảm ơn bạn';

  constructor() { }

  ngOnInit() {
    this.setIframeReady(this.iframe);
    this.dataBody$.next(this.emailBody);
    this.dataCallToAction$.next(this.callToAction);
    this.dataSignal$.next(this.signal);
  }

  /**
   * Send Data From Angular to Iframe
   */
  onChange(){
    this.dataBody$.next(this.emailBody);
    this.dataCallToAction$.next(this.callToAction);
    this.dataSignal$.next(this.signal);
  }

  private setIframeReady(iframe: ElementRef): void {
    const win: Window = iframe.nativeElement.contentWindow;

    // Pass the variable from parent to iframe
    win['emailBody'] = this.dataBody$;
    win['callToAction'] = this.dataCallToAction$;
    win['signal'] = this.dataSignal$;

    const doc: Document = win.document;
    doc.open();
    doc.write(this.previewHTML);
    doc.close()
  }

  onToggleView(width, height){
    this.viewMode = [width,height]
  }
}
