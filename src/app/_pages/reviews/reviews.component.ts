import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {
  edit_comment = false;
  customRanges = [
    {
      text: 'Hôm nay', desc: 'Hôm nay', value: 'today',
      start: moment(),
      end: moment(),
      default: true
    },
    {
      text: 'Hôm qua', desc: 'Hôm qua', value: 'lastday',
      start: moment().subtract(1, 'days').startOf('days'),
      end: moment().subtract(1, 'days').endOf('days')
    },
    {
      text: '7 ngày qua', desc: '7 ngày qua', value: 'last7day',
      start: moment().subtract(6, 'days').startOf('days'),
      end: moment().endOf('days')
    },
    {
      text: '30 ngày qua', desc: '30 ngày qua', value: 'last30day',
      start: moment().subtract(29, 'days').startOf('days'),
      end: moment().endOf('days')
    },
    {
      text: 'Tháng này', desc: 'Tháng này', value: 'thismonth',
      start: moment().startOf('month'),
      end: moment().endOf('month')
    },
    {
      text: 'Tháng trước', desc: 'Tháng trước', value: 'lastmonth',
      start: moment().subtract(1, 'month').startOf('month'),
      end: moment().subtract(1, 'month').endOf('month')
    }
  ];

  /**
   * Variable for table checked
   */
  masterSelected:boolean;
  checklist:any;
  checkedList = [];

  constructor() { }

  ngOnInit() {
    this.masterSelected = false;
    this.checklist = [
      {id:1,value:'Elenor Anderson',isSelected:false},
      {id:2,value:'Caden Kunze',isSelected:false},
      {id:3,value:'Ms. Hortense Zulauf',isSelected:false},
      {id:4,value:'Grady Reichert',isSelected:false},
      {id:5,value:'Dejon Olson',isSelected:false},
      {id:6,value:'Jamir Pfannerstill',isSelected:false},
      {id:7,value:'Aracely Renner DVM',isSelected:false},
      {id:8,value:'Genoveva Luettgen',isSelected:false}
    ];
  }

  checkUncheckAll() {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList();
  }
  isAllSelected() {
    this.masterSelected = this.checklist.every(function(item:any) {
        return item.isSelected == true;
      })
    this.getCheckedItemList();
  }
  getCheckedItemList(){
    this.checkedList = [];
    for (var i = 0; i < this.checklist.length; i++) {
      if(this.checklist[i].isSelected)
      this.checkedList.push(this.checklist[i]);
    }
    //this.checkedList = JSON.stringify(this.checkedList);
  }

  /**
   * Edit Comment Reviews
   */
  onEditComment(status: boolean){
    this.edit_comment = status;
    console.log(this.edit_comment)
  }
  /**
   * Show detail review
   */
  toggleShowDetails(index: number){
    this.checklist[index].showDetails = this.checklist[index].showDetails ? !this.checklist[index].showDetails : true
    // this.showDetails = !this.showDetails
  }
  /**
   * Filter By Datetime
   */
  isFilterByDate(event){
    
  }
}
