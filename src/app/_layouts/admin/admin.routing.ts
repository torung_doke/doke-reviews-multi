import { Routes } from '@angular/router';
import { DashboardComponent } from '../../_pages/dashboard/dashboard.component';
import { ReviewsComponent } from '../../_pages/reviews/reviews.component';
import { QuestionsComponent } from '../../_pages/questions/questions.component';
import { AnalyticsComponent } from '../../_pages/analytics/analytics.component';
import { CollectEmailComponent } from '../../_pages/collect-email/collect-email.component';
import { SettingComponent } from '../../_pages/setting/setting.component';
import { PlanComponent } from '../../_pages/plan/plan.component';
import { IntegrationsComponent } from '../../_pages/integrations/integrations.component';

export const AdminRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'reviews', component: ReviewsComponent},
    { path: 'questions', component: QuestionsComponent},
    { path: 'analytics', component: AnalyticsComponent},
    { path: 'email', component: CollectEmailComponent},
    { path: 'setting', component: SettingComponent},
    { path: 'plan', component: PlanComponent},
    { path: 'integrations', component: IntegrationsComponent},
    { path: '**', redirectTo: 'dashboard' }
];
