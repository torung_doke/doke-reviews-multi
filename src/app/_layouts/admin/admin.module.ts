import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { NgxDaterangepickerModule } from '@qqnc/ngx-daterangepicker';
import { AdminRoutes } from './admin.routing';
import { StarRatingComponent } from '../../_components/star-rating/star-rating.component';

import { DashboardComponent } from '../../_pages/dashboard/dashboard.component';
import { ReviewsComponent } from '../../_pages/reviews/reviews.component';
import { QuestionsComponent } from '../../_pages/questions/questions.component';
import { AnalyticsComponent } from '../../_pages/analytics/analytics.component';
import { SettingComponent } from '../../_pages/setting/setting.component';
import { PlanComponent } from '../../_pages/plan/plan.component';
import { CollectEmailComponent } from '../../_pages/collect-email/collect-email.component';
import { EscapeHtmlPipe } from '../../_pipes/keep-html.pipe';
import { IntegrationsComponent } from '../../_pages/integrations/integrations.component';



@NgModule({
  declarations: [
    DashboardComponent,
    ReviewsComponent,
    QuestionsComponent,
    StarRatingComponent,
    AnalyticsComponent,
    SettingComponent,
    PlanComponent,
    CollectEmailComponent,
    EscapeHtmlPipe,
    IntegrationsComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AdminRoutes),
    NgxDaterangepickerModule,
    // NgxDaterangepickerMd.forRoot(),
    NgbModule,
    FormsModule
  ]
})
export class AdminModule { }
