import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/admin/dashboard', title: 'Tổng quan',  icon: 'fa fa-home text-dark', class: '' },
    { path: '/admin/reviews', title: 'Đánh giá',  icon: 'fa fa-star-o text-dark', class: '' },
    { path: '/admin/questions', title: 'Câu hỏi',  icon: 'fa fa-comments-o text-dark', class: '' },
    { path: '/admin/analytics', title: 'Thống kê',  icon: 'fa fa-bar-chart text-dark', class: '' },
    { path: '/admin/email', title: 'Email đánh giá',  icon: 'fa fa-envelope text-dark', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
}
