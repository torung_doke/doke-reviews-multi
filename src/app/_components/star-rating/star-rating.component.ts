import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {
  star_off_number = 0;
  star_on: number[];
  star_off: number[];

  @Input('star') star: number;
  constructor() { }
  

  ngOnInit() {
    this.star_on = Array(this.star).map((x,i)=>i+1);
    this.star_off_number = 5 - this.star;
    this.star_off = Array(this.star_off_number).map((x,i)=>i);
  }

}
