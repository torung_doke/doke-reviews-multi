import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
//Interceptor
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {JwtInterceptor,ErrorInterceptor} from "./_helpers";
//Shared Module
import {SharedModule} from "./shared.module";
//Routing App
import { Routing } from './app.routing';
//Components
import { AppComponent } from './app.component';
import { ComponentsModule } from './_components/components.module';
import { AdminComponent } from './_layouts/admin/admin.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    Routing,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ComponentsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
