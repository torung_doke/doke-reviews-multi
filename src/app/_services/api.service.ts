import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { catchError } from "rxjs/operators";
import { Observable } from "rxjs/internal/Observable";
import { AuthenticationService } from "./authentication.service";
import { throwError } from "rxjs/internal/observable/throwError";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = environment.api_url;

  constructor(private http: HttpClient, private auth: AuthenticationService) { }

  public post(url: string, data: object): Observable<any> {
    return this.http.post(`${this.apiUrl}${url}`, data).pipe(catchError(this.handleError));
  }

  public get(url: string, params): Observable<any> {
      return this.http.get(`${this.apiUrl}${url}`, { params: params }).pipe(catchError(this.handleError));
  }

  public put(url: string, data: object): Observable<any> {
      return this.http.put(`${this.apiUrl}${url}`, data).pipe(catchError(this.handleError));
  }

  public delete(url: string): Observable<any> {
      return this.http.delete(`${this.apiUrl}${url}`).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status == 401) {
        this.auth.logout();
    } else {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            // console.error(
            //     `Backend returned code ${error.status}, ` +
            //     `body was: ${JSON.parse(error.error)}`);
        }
        let message = 'Something bad happened; please try again later.';
        if (error.error && error.error.message) {
            message = error.error.message
        }
        return throwError(message);
    }
  };
}
