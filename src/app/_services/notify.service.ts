import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Constants } from '../_share';
@Injectable({
  providedIn: 'root'
})
export class NotifyService {
  private NOTIFY = Constants.NOTIFY;

  private toastr_option = {
    timeOut: 2000,
    tapToDismiss: false,
    positionClass: 'toast-bottom-center',
    closeButton: true
  }
  constructor(private toastr: ToastrService) { }

  isSuccess() {
    this.toastr.success(this.NOTIFY.SUCCESS, '', this.toastr_option);
  }
  isError() {
    this.toastr.error(this.NOTIFY.ERROR, '', this.toastr_option);
  }
}
