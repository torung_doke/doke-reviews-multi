import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Constants } from '../_share';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  public logout(): Observable<any> {
    console.log('Logout');
    return 
  }

  public getToken(){
    let token  = localStorage.getItem(Constants.TOKEN);
    return token
  }
}
