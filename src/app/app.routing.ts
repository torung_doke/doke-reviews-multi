import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './_layouts/admin/admin.component';
// import { LoginComponent } from './login';
// import { AuthGuard } from './_guards';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: 'admin',
        pathMatch: 'full'
        // canActivate: [AuthGuard]
    }, {
        path: 'admin',
        component: AdminComponent,
        children: [
          {
            path: '',
            loadChildren: './_layouts/admin/admin.module#AdminModule'
          }
        ]
    },
    // {
    //     path: 'login',
    //     component: LoginComponent
    // },

    // otherwise redirect to home
    { path: '**', redirectTo: 'admin' }
];

export const Routing = RouterModule.forRoot(appRoutes);